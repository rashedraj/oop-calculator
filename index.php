<?php 
	include_once('header.php');
	include_once('function.php');
 ?>	


	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3 well">
				<h3 class="text-center">Calculator using OOP</h3> 
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3 well">
				<form action="" method="post">
					<div class="form-group">
						<label for="first_number">First Number</label>
						<input type="number" class="form-control" id="first_number" placeholder="First Number" name="first_number">
					</div>
					<div class="form-group">
						<label for="second_number">Email address</label>
						<input type="number" class="form-control" id="second_number" placeholder="Second Number" name="second_number">
					</div>
					<button type="submit" class="btn btn-block btn-info" name="calculate">Calculate</button>
				</form>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3 well">
				<?php 
					if (isset($_POST['calculate'])) {
						$new_first_number 	= $_POST['first_number'];
						$new_second_number 	= $_POST['second_number'];
						if (empty($new_first_number) or empty($new_second_number)) {
							echo "<div class='alert alert-warning' role='alert'>"."Field Must Not Be Empty !!"."</div>";	
						}else{
							echo "<h3 class='text-center'>"."First Number Is: ".$new_first_number."</h3>";
							echo "<h3 class='text-center'>"."Second Number Is: ".$new_second_number."</h3>";
							$cal = new Calculation;
							$cal->add($new_first_number,$new_second_number);
							$cal->sub($new_first_number,$new_second_number);
							$cal->mul($new_first_number,$new_second_number);
							$cal->div($new_first_number,$new_second_number);
						}			
					}
				 ?>
			</div>
		</div>
			
	</div>



<?php
	include_once('footer.php');
 ?>